# Inuits DAMS LDES importer

This is a service to import objects from an event stream into the DAMS collection-api, and link them with mediafiles in the DAMS storage-api.

## Running inside local dev environment

A script, run-import.sh, is provided in the scripts folder of the inuits-dams-docker repo.

Make sure the container is built by running

```
./scripts/docker-compose-dev.sh --profile import build
```
inside the root of the inuits-dams-docker repo.

Run the import:
```
./scripts/run-import.sh EVENTSTREAM_URL MEDIAFILES_URL
```

The `EVENTSTREAM_URL` has to be a url to the output of an event stream.
The `MEDIAFILES_URL` can optionally be a url from which mediafiles can be downloaded and matched with the id of the objects from the event stream output.

## Running standalone

### Dev version

Make sure the container is built by running

```
./scripts/build-dev-container.sh
```

Run the import:
```
./scripts/dev-container.sh EVENSTREAM_URL MEDIAFILES_URL
```

### Prod version

Make sure the container is built by running

```
./scripts/build-run-container.sh
```

Run the import:
```
./scripts/run-container.sh EVENSTREAM_URL MEDIAFILES_URL
```
