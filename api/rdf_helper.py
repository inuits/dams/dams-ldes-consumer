from rdflib import Graph, Literal, namespace, URIRef

default_lang = "nl"


class RdfHelper:
    def __init__(self, language=default_lang):
        self.graph = Graph()
        self.language = language

    def get_pref_label(self, url):
        self.graph.parse(url, format="xml")

        def filter_labels(node):
            return isinstance(node, Literal) and node.language == self.language

        pref_label = next(
            (
                filter(
                    filter_labels,
                    self.graph.objects(URIRef(url), namespace.SKOS.prefLabel),
                )
            ),
            None,
        )
        if not pref_label:
            pref_label = next(
                (
                    filter(
                        filter_labels,
                        self.graph.objects(URIRef(url), namespace.RDFS.label),
                    )
                ),
                None,
            )
        return pref_label
