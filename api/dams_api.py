import requests

from exceptions import NonUniqueException, NotFoundException
from time import sleep


class DamsApi:
    def __init__(self, collection_api_url, static_jwt="", request_delay=0.1):
        self.collection_api_url = collection_api_url
        self.headers = {"Authorization": f"Bearer {static_jwt}"}
        self.request_delay = float(request_delay)

    def __sleep(self):
        if self.request_delay > 0:
            sleep(self.request_delay)

    def __handle_response(self, response, error_message, update=True):
        if response.status_code == 409:
            raise NonUniqueException(response.text.strip())
        elif update and response.status_code == 404:
            raise NotFoundException(response.text.strip())
        elif response.status_code != 201:
            raise Exception(f"{error_message}: {response.text.strip()}")
        self.__sleep()
        return response.json()

    def add_object(self, collection, payload):
        url = f"{self.collection_api_url}/{collection}"
        response = requests.post(url, json=payload, headers=self.headers)
        return self.__handle_response(response, "Failed to add object", False)

    def get_object(self, collection, identifier):
        url = f"{self.collection_api_url}/{collection}/{identifier}"
        response = requests.get(url, headers=self.headers)
        if response.status_code != 404 and response.status_code != 200:
            raise Exception(f"Failed to get object: {response.text.strip()}")
        self.__sleep()
        return response.json() if response.status_code == 200 else ""

    def update_object(self, collection, identifier, payload):
        url = f"{self.collection_api_url}/{collection}/{identifier}"
        response = requests.put(url, json=payload, headers=self.headers)
        return self.__handle_response(response, "Failed to update object")

    def update_object_relations(self, collection, identifier, payload):
        url = f"{self.collection_api_url}/{collection}/{identifier}/relations"
        response = requests.patch(url, json=payload, headers=self.headers)
        return self.__handle_response(response, "Failed to update object relations")

    def delete_object(self, collection, identifier):
        url = f"{self.collection_api_url}/{collection}/{identifier}"
        response = requests.delete(url, headers=self.headers)
        if response.status_code != 204:
            raise Exception(f"Failed to delete object: {response.text.strip()}")
        self.__sleep()
