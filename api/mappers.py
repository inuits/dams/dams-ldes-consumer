from pint import errors, UnitRegistry
from rdf_helper import RdfHelper

default_lang = "nl"
rdf_helper = RdfHelper()

ureg = UnitRegistry()


def add_float_data_for_dimensions(metadata):
    if not metadata:
        return
    label = metadata[-1]["key"]
    value = metadata[-1]["value"]
    lang = metadata[-1].get("lang", "")
    if label in [
        "height",
        "width",
        "depth",
        "length",
        "diameter",
    ]:
        try:
            metadata.append(
                {
                    "key": f"{label}_float_value",
                    "value": ureg.Quantity(value).to_base_units().magnitude,
                    "lang": lang,
                }
            )
        except:
            pass


def construct_id(label, ld_object):
    return ld_object if isinstance(ld_object, list) else [ld_object]


def construct_ld_metadata(label, ld_object):
    label_ldes = {
        "title": "MensgemaaktObject.titel",
        "description": "Entiteit.beschrijving",
    }.get(label, label)
    lang = ld_object["@language"] if "@language" in ld_object else default_lang
    return [
        {"key": label, "value": ld_object["@value"], "lang": lang, "label": label_ldes}
    ]


def construct_object_number(label, ld_object):
    identifier_string = "https://stad.gent/id/identificatiesysteem/objectnummer"
    identifier = None
    for item in ld_object:
        if item["Identificator.identificator"]["@type"] == identifier_string:
            identifier = item
    if not identifier:
        return []
    lang = (
        identifier["Identificator.identificator"]["@language"]
        if "@language" in identifier["Identificator.identificator"]
        else default_lang
    )
    return [
        {
            "key": label,
            "value": identifier["Identificator.identificator"]["@value"],
            "lang": lang,
            "label": identifier["Entiteit.type"]["label"]
            if "Entiteit.type" in identifier
            else label,
        }
    ]


def construct_simple_metadata(label, ld_object):
    label_ldes = {
        "alternativeName": "alternatieveNaam",
        "firstname": "voornaam",
        "lastname": "achternaam",
        "fullname": "volledigeNaam",
    }.get(label, label)
    if isinstance(ld_object, list):
        ld_object = ",".join(ld_object)
    return [
        {"key": label, "value": ld_object, "label": label_ldes, "lang": default_lang}
    ]


def construct_nationality(label, ld_object):
    if (
        "Entiteit.beschrijving" not in ld_object
        or "@type" not in ld_object
        or ld_object["@type"] != "Nationaliteit"
    ):
        return []
    lang = (
        ld_object["Entiteit.beschrijving"]["@language"]
        if "@language" in ld_object["Entiteit.beschrijving"]
        else default_lang
    )
    return [
        {
            "key": label,
            "value": ld_object["Entiteit.beschrijving"]["@value"],
            "label": "heeftNationaliteit",
            "lang": lang,
        }
    ]


def construct_birth_death_metadata(label, ld_object):
    metadata = []
    if "datum" in ld_object:
        date_key = "date_of_birth" if label == "hasBirth" else "date_of_death"
        metadata.append(
            {"key": date_key, "value": ld_object["datum"], "lang": default_lang}
        )
    if "plaats" in ld_object:
        place_key = "place_of_birth" if label == "hasBirth" else "place_of_death"
        metadata.append(
            {
                "key": place_key,
                "value": ld_object["plaats"],
                "lang": default_lang,
            }
        )
    return metadata


def construct_timing(label, ld_object):
    metadata = list()
    edtf_string = "http://id.loc.gov/datatypes/edtf/EDTF"
    if "@type" in ld_object and ld_object["@type"] == edtf_string:
        metadata.append(
            {
                "key": label,
                "value": ld_object["@value"],
                "lang": ld_object["@language"]
                if "@language" in ld_object
                else default_lang,
                "label": "Gebeurtenis.tijd",
                "format": "EDTF",
            }
        )
    elif "@type" in ld_object and ld_object["@type"] == "Periode":
        metadata.append(
            {
                "key": "period_start",
                "value": ld_object["Periode.begin"],
                "lang": ld_object["@language"]
                if "@language" in ld_object
                else default_lang,
                "label": "Periode.begin",
                "format": "EDTF",
            }
        )
        metadata.append(
            {
                "key": "period_end",
                "value": ld_object["Periode.einde"],
                "lang": ld_object["@language"]
                if "@language" in ld_object
                else default_lang,
                "label": "Periode.einde",
                "format": "EDTF",
            }
        )
    elif isinstance(ld_object, str):
        metadata.append(
            {
                "key": "date",
                "value": ld_object,
                "lang": default_lang,
                "label": "datum",
                "format": "EDTF",
            }
        )
    return metadata


def construct_place(label, ld_object):
    metadata = list()
    if (
        "@type" in ld_object
        and ld_object["@type"] == "Plaats"
        and "equivalent" in ld_object
    ):
        metadata.append(
            {
                "key": label,
                "value": ld_object["equivalent"]["skos:prefLabel"]["@value"],
                "lang": ld_object["equivalent"]["skos:prefLabel"]["@language"]
                if "@language" in ld_object["equivalent"]["skos:prefLabel"]
                else default_lang,
                "label": ld_object["Entiteit.type"]["label"],
            }
        )
    if "@type" in ld_object and ld_object["@type"] == "Plaats" and "label" in ld_object:
        metadata.append(
            {
                "key": label,
                "value": ld_object["label"]["@value"],
                "lang": ld_object["label"]["@language"]
                if "@language" in ld_object["label"]
                else default_lang,
                "label": "plaats",
            }
        )
    elif "@type" in ld_object and ld_object["@type"] == "Locatie":
        metadata.append(
            {
                "key": label,
                "value": ld_object["Entiteit.type"][0]["skos:prefLabel"]["@value"],
                "lang": ld_object["Entiteit.type"][0]["skos:prefLabel"]["@language"]
                if "@language" in ld_object["Entiteit.type"][0]["skos:prefLabel"]
                else default_lang,
                "label": ld_object["Entiteit.type"][1]["label"],
            }
        )

    return metadata


def construct_labeled_entity_type(label, ld_object):
    metadata = list()
    if not isinstance(ld_object, list):
        return metadata
    for item in ld_object:
        for entity in item["Entiteit.type"]:
            if "skos:prefLabel" in entity:
                metadata.append(
                    {
                        "key": label,
                        "value": entity["skos:prefLabel"]["@value"],
                        "lang": entity["skos:prefLabel"]["@language"]
                        if "@language" in entity["skos:prefLabel"]
                        else default_lang,
                    }
                )
            elif "label" in entity:
                metadata.append({"label": entity["label"]})
    return metadata


# FIXME: change mapping to accept new dimension definitions
def construct_dimension(label, ld_object):
    metadata = list()
    for dimension in ld_object:
        if "@type" not in dimension or dimension["@type"] != "Dimensie":
            continue
        if any(
            x not in dimension
            for x in ["Dimensie.type", "Dimensie.waarde", "Dimensie.eenheid"]
        ):
            continue
        mapping = {
            "hoogte": "height",
            "breedte": "width",
            "diepte": "depth",
            "lengte": "length",
        }
        metadata.append(
            {
                "key": mapping.get(
                    dimension["Dimensie.type"], dimension["Dimensie.type"]
                ),
                "value": f'{dimension["Dimensie.waarde"]} {dimension["Dimensie.eenheid"]}',
                "lang": default_lang,
                "label": dimension["Dimensie.type"],
            }
        )
        add_float_data_for_dimensions(metadata)
    return metadata


def construct_description(label, ld_object):
    label_ldes = {"description": "Entiteit.beschrijving"}.get(label, label)
    return [
        {
            "key": label,
            "value": ld_object if isinstance(ld_object, str) else ld_object["@value"],
            "lang": ld_object["@language"]
            if "@language" in ld_object
            else default_lang,
            "label": label_ldes,
        }
    ]


def construct_component(ld_object, type, new_id):
    metadata = get_mapped_fields(ld_object, metadata_mapping_component)
    post_data = {
        "type": type,
        "metadata": metadata,
        "data": ld_object,
        "identifiers": [new_id],
        "object_id": new_id,
    }
    return post_data


metadata_labels_asset = [
    "type",
    "title",
    "description",
    "material",
    "creation_date",
    "height",
    "width",
    "depth",
    "length",
    "diameter",
]

metadata_labels_thesaurus = ["title", "scopeNote"]

metadata_labels_person = [
    "fullname",
    "alternativeName",
    "firstname",
    "lastname",
    "nationality",
    "skos:note",
]

metadata_mapping_asset = {
    "MensgemaaktObject.titel": {"label": "title", "mapper": construct_ld_metadata},
    "Entiteit.beschrijving": {"label": "description", "mapper": construct_ld_metadata},
    "MensgemaaktObject.dimensie": {"label": "dimension", "mapper": construct_dimension},
    "Object.identificator": {
        "label": "object_number",
        "mapper": construct_object_number,
    },
}

metadata_mapping_component = {
    "Entiteit.beschrijving": {"label": "description", "mapper": construct_description},
    "Gebeurtenis.tijd": {"label": "event_time", "mapper": construct_timing},
    "datum": {"label": "event_time", "mapper": construct_timing},
    "MensgemaaktObject.dimensie": {"label": "dimension", "mapper": construct_dimension},
}

metadata_mapping_person = {
    "volledigeNaam": {"label": "fullname", "mapper": construct_simple_metadata},
    "alternatieveNaam": {
        "label": "alternativeName",
        "mapper": construct_simple_metadata,
    },
    "voornaam": {"label": "firstname", "mapper": construct_simple_metadata},
    "achternaam": {"label": "lastname", "mapper": construct_simple_metadata},
    "skos:note": {"label": "skos:note", "mapper": construct_simple_metadata},
    "heeftNationaliteit": {"label": "nationality", "mapper": construct_nationality},
}

metadata_mapping_thesaurus = {
    "skos:scopeNote": {"label": "scopeNote", "mapper": construct_simple_metadata},
    "skos:prefLabel": {"label": "title", "mapper": construct_ld_metadata},
}

identifier_mapping = {
    "@id": {
        "label": "id",
        "mapper": construct_id,
    },
    "dcterms:isVersionOf": {
        "label": "id",
        "mapper": construct_id,
    },
    "Identificator.identificator": {
        "label": "id",
        "mapper": construct_id,
    },
}

relation_fields = {
    "MensgemaaktObject.maaktDeelUitVan": {"type": "getty"},
    "MaterieelDing.beheerder": {"type": "museum"},
    "prov:wasAttributedTo": {"type": "museum"},
    "MaterieelDing.bestaatUit": {"type": "component", "subtype": "consists_of"},
    "Activiteit.uitgevoerdDoor": {"type": "person"},
    "MaterieelDing.isOvergedragenBijVerwerving": {
        "type": "component",
        "subtype": "aquisition",
    },
    "Entiteit.classificatie": {"type": "component", "subtype": "classification"},
    "Classificatie.toegekendType": {"type": "getty"},
    "Entiteit.wordtNaarVerwezenDoor": {"type": "component", "subtype": "redirected_by"},
    "Entiteit.maaktDeelUitVan": {"type": "component", "subtype": "is_part_of"},
    "ConceptueelDing.heeftCreatie": {"type": "component", "subtype": "has_creation"},
    "MaterieelDing.productie": {"type": "component", "subtype": "production"},
    "gebruiktBijActiviteit": {"type": "component", "subtype": "activity"},
    "MensgemaaktObject.draagt": {"type": "component", "subtype": "wears"},
    "Verwerving.overgedragenAan": {"type": "museum"},
    "Activiteit.gebruikteTechniek": {"type": "getty"},
    "heeftGeboorte": {"type": "component", "subtype": "birth"},
    "heeftOverlijden": {"type": "component", "subtype": "death"},
    "InformatieObject.gaatOver": {"type": "getty"},
    "Gebeurtenis.plaats": {"type": "getty"},
    "plaats": {"type": "getty"},
    "MensgemaaktObject.beeldtUit": {"type": "getty"},
    "InformatieObject.verwijstNaar": {"type": "getty"},
    "MensgemaaktObject.materiaal": {"type": "getty"},
    "geslacht": {"type": "gender"},
    "GecureerdeCollectie.bestaatUit": {"type": "sub_assets"},
    "Rol.activiteit": {"type": "reverse_component"},
}


def _make_metadata_immutable(metadata_list, mapping):
    if mapping == metadata_mapping_person:
        metadata_labels = metadata_labels_person
    elif mapping == metadata_mapping_thesaurus:
        metadata_labels = metadata_labels_thesaurus
    elif mapping == metadata_mapping_asset:
        metadata_labels = metadata_labels_asset
    else:
        return
    existing_metadata_labels = set()
    for metadata in metadata_list:
        metadata["immutable"] = True
        existing_metadata_labels.add(metadata["key"])
    for label in [x for x in metadata_labels if x not in existing_metadata_labels]:
        metadata_list.append(
            {"key": label, "value": "", "lang": "", "label": "", "immutable": True}
        )


def get_mapped_fields(asset, mapping):
    fields = list()
    for oslo_key, value in asset.items():
        if oslo_key in mapping:
            fields_mapping = mapping[oslo_key]
            fields.extend(
                fields_mapping["mapper"](fields_mapping["label"], asset[oslo_key])
            )
    _make_metadata_immutable(fields, mapping)
    return fields
