import base64
import logging
import mappers
import os
import requests
import sys

from dams_api import DamsApi
from exceptions import NonUniqueException, NotFoundException
from multiprocessing import Process
from rdf_helper import RdfHelper
from wikidata_resolver import WikidataResolver

collection_api = DamsApi(
    os.getenv("COLLECTION_API_URL"),
    os.getenv("STATIC_JWT"),
    os.getenv("REQUEST_DELAY"),
)
logging.basicConfig(
    format="%(asctime)s %(process)d,%(threadName)s %(filename)s:%(lineno)d [%(levelname)s] %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
    level=logging.INFO,
)
logger = logging.getLogger(__name__)
skip_private_streams = os.getenv("SKIP_PRIVATE_STREAMS", True)
stream_base_urls = {
    "public": os.getenv("EVENT_STREAM_BASE_URL"),
    "private": os.getenv("EVENT_STREAM_PRIVATE_URL"),
}
sync_streams = {
    os.getenv("THESAURUS_STREAM", "/adlib/thesaurus"): "adlib_thesaurus",
    os.getenv("PERSONEN_STREAM", "/adlib/personen"): "adlib_personen",
}
async_streams = {
    os.getenv("ARCHIEFGENT_STREAM", "/archiefgent/objecten"): "archiefgent",
    os.getenv("DMG_STREAM", "/dmg/objecten"): "dmg",
    os.getenv("DMG_PRIVATE_STREAM", "/dmg/objecten"): {
        "stream_id": "dmg_private",
        "object_id_prefix": "dmg",
        "private": True,
    },
    os.getenv("DMG_STREAM_2", "/dmg/archief"): {
        "stream_id": "dmg2",
        "object_id_prefix": "dmg",
    },
    os.getenv("HVA_STREAM", "/hva/objecten"): "hva",
    os.getenv("INDUSTRIEMUSEUM_STREAM", "/industriemuseum/objecten"): "industriemuseum",
    os.getenv("STAM_STREAM", "/stam/objecten"): "stam",
}
rdf_helper = RdfHelper()
wikidata = WikidataResolver(collection_api)
asset_cache = dict()
gender_cache = dict()
getty_cache = dict()
person_cache = dict()
role_cache = dict()
thesaurus_cache = dict()
wikidata_cache = dict()


def save_thesaurus(asset, object_id_prefix, private):
    original_data = asset
    metadata = mappers.get_mapped_fields(asset, mappers.metadata_mapping_thesaurus)
    if private:
        metadata.append({"key": "publication_status", "value": "niet-publiek"})
    identifiers = mappers.get_mapped_fields(asset, mappers.identifier_mapping)
    id = base64.b64encode(asset["dcterms:isVersionOf"].encode("ascii")).decode("ascii")
    identifiers.append(id)
    if "Object.identificator" in asset:
        for identificator_object in asset["Object.identificator"]:
            if (
                identificator_object["Identificator.identificator"]["@type"]
                == "https://stad.gent/id/identificatiesysteem/objectnummer"
            ):
                identifiers.append(
                    object_id_prefix
                    + identificator_object["Identificator.identificator"]["@value"]
                )
                break
    post_data = {
        "identifiers": identifiers,
        "object_id": id,
        "type": "thesaurus",
        "metadata": metadata,
        "data": original_data,
    }

    if not collection_api.get_object("entities", id):
        collection_api.add_object("entities", post_data)
    else:
        post_data.pop("identifiers", None)
        collection_api.update_object("entities", id, post_data)


def save_person(asset, object_id_prefix, private):
    original_data = asset
    metadata = mappers.get_mapped_fields(asset, mappers.metadata_mapping_person)
    if private:
        metadata.append({"key": "publication_status", "value": "niet-publiek"})
    identifiers = mappers.get_mapped_fields(asset, mappers.identifier_mapping)
    if "Object.identificator" in asset:
        for identificator_object in asset["Object.identificator"]:
            if (
                identificator_object["Identificator.identificator"]["@type"]
                == "https://stad.gent/id/identificatiesysteem/objectnummer"
            ):
                identifiers.append(
                    object_id_prefix
                    + identificator_object["Identificator.identificator"]["@value"]
                )
                break
    if "dcterms:isVersionOf" in asset:
        id = base64.b64encode(asset["dcterms:isVersionOf"].encode("ascii")).decode(
            "ascii"
        )
        identifiers.append(id)
        post_data = {
            "identifiers": identifiers,
            "object_id": id,
            "type": "person",
            "metadata": metadata,
            "data": original_data,
        }
        if not collection_api.get_object("entities", id):
            collection_api.add_object("entities", post_data)
        else:
            post_data.pop("identifiers", None)
            collection_api.update_object("entities", id, post_data)
    map_relations(asset, id)


def save_asset(asset, object_id_prefix, private):
    original_data = asset
    metadata = mappers.get_mapped_fields(asset, mappers.metadata_mapping_asset)
    if private:
        metadata.append({"key": "publication_status", "value": "niet-publiek"})
    identifiers = mappers.get_mapped_fields(asset, mappers.identifier_mapping)
    object_id = ""
    if "Object.identificator" in asset:
        for identificator_object in asset["Object.identificator"]:
            if (
                identificator_object["Identificator.identificator"]["@type"]
                == "https://stad.gent/id/identificatiesysteem/objectnummer"
            ):
                object_id = f'{object_id_prefix}{identificator_object["Identificator.identificator"]["@value"]}'
                identifiers.append(object_id)
                logger.info(f"{object_id}")
                break
    id = ""
    if "dcterms:isVersionOf" in asset:
        id = base64.b64encode(asset["dcterms:isVersionOf"].encode("ascii")).decode(
            "ascii"
        )
        identifiers.append(id)
    post_data = {
        "identifiers": identifiers,
        "type": "asset",
        "metadata": metadata,
        "data": original_data,
        "object_id": object_id,
    }
    if not object_id or "?" in object_id or not id:
        logger.info(f"Skipping object_id {object_id} priref {id}")
        return
    if (existing_asset := collection_api.get_object("entities", id)) or (
        existing_asset := collection_api.get_object("entities", object_id)
    ):
        post_data["identifiers"].insert(0, existing_asset["identifiers"][0])
        try:
            collection_api.update_object("entities", id, post_data)
        except NotFoundException:
            collection_api.update_object("entities", object_id, post_data)
        except NonUniqueException:
            collection_api.delete_object("entities", id)
            collection_api.update_object("entities", object_id, post_data)

    else:
        collection_api.add_object("entities", post_data)
    map_relations(asset, object_id)


def map_relations(asset, object_id):
    if "data" in asset:
        asset = asset["data"]
    for relation_field, relation in mappers.relation_fields.items():
        if relation["type"] == "museum":
            if relation_field in asset:
                if "isEenheidVan" in asset[relation_field]:
                    museum_id = asset[relation_field]["isEenheidVan"].split("/")[-1]
                elif "@id" in asset[relation_field]:
                    museum_id = asset[relation_field]["@id"].split("/")[-1]
                else:
                    museum_id = asset[relation_field].split("/")[-1]
                if museum_id in wikidata_cache:
                    museum_entity = wikidata_cache[museum_id]
                else:
                    museum_entity = wikidata.get_and_update_wiki_data_entity_from_db(
                        museum_id
                    )
                    wikidata_cache[museum_id] = museum_entity
                if museum_entity:
                    label = ""
                    for metadata in museum_entity["metadata"]:
                        if metadata["key"] == "title":
                            label = metadata["value"]
                    collection_api.update_object_relations(
                        "entities",
                        object_id,
                        [
                            {
                                "label": relation_field,
                                "value": label,
                                "key": museum_entity["_id"],
                                "type": "isIn",
                            }
                        ],
                    )
        elif relation["type"] == "getty":
            if relation_field in asset:
                if isinstance(asset[relation_field], dict):
                    asset[relation_field] = [asset[relation_field]]
                for getty_item in asset[relation_field]:
                    if "@type" not in getty_item or (
                        "@type" in getty_item
                        and getty_item["@type"] != "GecureerdeCollectie"
                    ):
                        getty_id = None
                        getty_label = ""
                        getty_lang = ""
                        getty_value = ""
                        type = "getty"
                        if (
                            "@type" in getty_item
                            and getty_item["@type"]
                            in ["Entiteit", "TypeTechniek", "Locatie", "TypeMateriaal"]
                            and "Entiteit.type" in getty_item
                        ):
                            if (
                                "@id" in getty_item["Entiteit.type"][0]
                                and "skos:prefLabel" in getty_item["Entiteit.type"][0]
                                and getty_item["Entiteit.type"][0]["@id"]
                                != "https://stad.gent/id/concept/"
                            ):
                                getty_id = base64.b64encode(
                                    getty_item["Entiteit.type"][0]["@id"].encode(
                                        "ascii"
                                    )
                                ).decode("ascii")
                                getty_label = getty_item["Entiteit.type"][1]["label"]
                                getty_value = getty_item["Entiteit.type"][0][
                                    "skos:prefLabel"
                                ]["@value"]
                                getty_lang = getty_item["Entiteit.type"][0][
                                    "skos:prefLabel"
                                ]["@language"]
                            elif (
                                "@id" in getty_item["Entiteit.type"][1]
                                and "skos:prefLabel" in getty_item["Entiteit.type"][1]
                                and getty_item["Entiteit.type"][1]["@id"]
                                != "https://stad.gent/id/concept/"
                            ):
                                getty_id = base64.b64encode(
                                    getty_item["Entiteit.type"][1]["@id"].encode(
                                        "ascii"
                                    )
                                ).decode("ascii")
                                getty_label = getty_item["Entiteit.type"][0]["label"]
                                getty_value = getty_item["Entiteit.type"][1][
                                    "skos:prefLabel"
                                ]["@value"]
                                getty_lang = getty_item["Entiteit.type"][1][
                                    "skos:prefLabel"
                                ]["@language"]
                        if (
                            "@type" in getty_item
                            and getty_item["@type"] in ["Persoon"]
                            and "Entiteit.type" in getty_item
                        ):
                            if "@id" in getty_item["Entiteit.type"][0]:
                                getty_id = base64.b64encode(
                                    getty_item["Entiteit.type"][0]["@id"].encode(
                                        "ascii"
                                    )
                                ).decode("ascii")
                                getty_label = getty_item["Entiteit.type"][1]["label"]
                                getty_value = (
                                    getty_item["Entiteit.type"][0]["label"]["@value"]
                                    if "@value"
                                    in getty_item["Entiteit.type"][0]["label"]
                                    else getty_item["Entiteit.type"][0]["label"]
                                )
                                getty_lang = (
                                    getty_item["Entiteit.type"][0]["label"]["@language"]
                                    if "@language"
                                    in getty_item["Entiteit.type"][0]["label"]
                                    else "nl"
                                )
                                type = "person"
                        elif (
                            "@type" in getty_item
                            and getty_item["@type"] == "Plaats"
                            and "equivalent" in getty_item
                        ):
                            getty_id = base64.b64encode(
                                getty_item["equivalent"]["@id"].encode("ascii")
                            ).decode("ascii")
                            getty_label = getty_item["Entiteit.type"]["label"]
                            getty_value = getty_item["equivalent"]["skos:prefLabel"][
                                "@value"
                            ]
                            getty_lang = getty_item["equivalent"]["skos:prefLabel"][
                                "@language"
                            ]
                        elif (
                            "@type" in getty_item
                            and getty_item["@type"] == "Plaats"
                            and "label" in getty_item
                        ):
                            getty_id = base64.b64encode(
                                getty_item["@id"].encode("ascii")
                            ).decode("ascii")
                            getty_label = "plaats"
                            getty_value = getty_item["label"]["@value"]
                            getty_lang = getty_item["label"]["@language"]
                        elif (
                            "@type" in getty_item
                            and getty_item["@type"] == "Collectie"
                            and "Entiteit.beschrijving" in getty_item
                        ):
                            getty_id = base64.b64encode(
                                getty_item["@id"].encode("ascii")
                            ).decode("ascii")
                            if len(getty_item["Entiteit.type"]) == 2:
                                getty_label = getty_item["Entiteit.type"][1]["label"]
                            else:
                                getty_label = getty_item["Entiteit.type"][0]["label"]

                            getty_value = getty_item["Entiteit.beschrijving"]
                            getty_lang = "nl"
                        elif (
                            relation_field == "Classificatie.toegekendType"
                            and "Entiteit.type" in asset
                            and isinstance(asset["Entiteit.type"], dict)
                        ):
                            getty_id = base64.b64encode(
                                getty_item["@id"].encode("ascii")
                            ).decode("ascii")
                            getty_label = asset["Entiteit.type"]["label"]
                            getty_value = getty_item["skos:prefLabel"]["@value"]
                            getty_lang = getty_item["skos:prefLabel"]["@language"]
                        elif relation_field == "Classificatie.toegekendType":
                            getty_id = base64.b64encode(
                                getty_item["@id"].encode("ascii")
                            ).decode("ascii")
                            getty_label = relation_field
                            if "skos:prefLabel" in getty_item:
                                getty_value = getty_item["skos:prefLabel"]["@value"]
                                getty_lang = getty_item["skos:prefLabel"]["@language"]
                            else:
                                getty_value = getty_item["label"]["@value"]
                                getty_lang = getty_item["label"]["@language"]
                        if getty_id in getty_cache:
                            getty_entity = getty_cache[getty_id]
                        else:
                            getty_entity = collection_api.get_object(
                                "entities", getty_id
                            )
                        if "_id" not in getty_entity and getty_id is not None:
                            post_data = {
                                "identifiers": [getty_id],
                                "type": type,
                                "metadata": [
                                    {
                                        "key": "title"
                                        if type != "person"
                                        else "fullname",
                                        "label": getty_label,
                                        "value": getty_value,
                                        "lang": getty_lang,
                                    }
                                ],
                                "data": {
                                    "title"
                                    if type != "person"
                                    else "fullname": getty_value
                                },
                            }
                            if type == "getty":
                                post_data["object_id"] = getty_id
                            getty_entity = collection_api.add_object(
                                "entities", post_data
                            )
                        getty_cache[getty_id] = getty_entity
                        if "_id" in getty_entity:
                            collection_api.update_object_relations(
                                "entities",
                                object_id,
                                [
                                    {
                                        "label": getty_label,
                                        "value": getty_value,
                                        "key": getty_entity["_id"],
                                        "type": "components",
                                    }
                                ],
                            )
        elif relation["type"] == "component" or relation["type"] == "reverse_component":
            if relation["type"] == "reverse_component" and "@reverse" in asset:
                if "Rol.rol" in asset["@reverse"][relation_field]:
                    role_component = asset["@reverse"][relation_field]["Rol.rol"]
                    role_component_entity = False
                    label = relation_field
                    value = ""
                    if "@id" in role_component:
                        role_id = base64.b64encode(
                            role_component["@id"].encode("ascii")
                        ).decode("ascii")
                        value = role_component["skos:prefLabel"]["@value"]
                        if role_id in role_cache:
                            role_component_entity = role_cache[role_id]
                        else:
                            role_component_entity = collection_api.get_object(
                                "entities", role_id
                            )
                        if "_id" not in role_component_entity:
                            post_data = {
                                "identifiers": [role_id],
                                "object_id": role_id,
                                "type": "role",
                                "metadata": [
                                    {
                                        "key": "title",
                                        "label": "titel",
                                        "value": value,
                                        "lang": role_component["skos:prefLabel"][
                                            "@language"
                                        ],
                                    }
                                ],
                                "data": {
                                    "title": role_component["skos:prefLabel"]["@value"]
                                },
                            }
                            role_component_entity = collection_api.add_object(
                                "entities", post_data
                            )
                            role_cache[role_id] = role_component_entity
                    if "Entiteit.type" in asset["@reverse"][relation_field]:
                        label = asset["@reverse"][relation_field]["Entiteit.type"][
                            "label"
                        ]
                    if role_component_entity and "_id" in role_component_entity:
                        collection_api.update_object_relations(
                            "entities",
                            object_id,
                            [
                                {
                                    "label": label,
                                    "value": value,
                                    "key": role_component_entity["_id"],
                                    "type": "components",
                                }
                            ],
                        )
            elif relation_field in asset:
                if isinstance(asset[relation_field], dict):
                    component_list = [asset[relation_field]]
                else:
                    component_list = asset[relation_field]
                sub_id_count = 0
                label = relation_field
                value = ""
                for component in component_list:
                    component_entity = False
                    if (
                        "@id" in component
                        and "https://stad.gent/id/concept" in component["@id"]
                    ):
                        thesaurus_id = base64.b64encode(
                            component["@id"].encode("ascii")
                        ).decode("ascii")
                        logger.info(thesaurus_id)
                        if thesaurus_id in thesaurus_cache:
                            component_entity = thesaurus_cache[thesaurus_id]
                        else:
                            component_entity = collection_api.get_object(
                                "entities", thesaurus_id
                            )
                            thesaurus_cache[thesaurus_id] = component_entity

                        if "data" in component_entity:
                            if "Entiteit.type" in component and isinstance(
                                component["Entiteit.type"], list
                            ):
                                for entity_type_object in component["Entiteit.type"]:
                                    if (
                                        "@id" in entity_type_object
                                        and "cest:" in entity_type_object["@id"]
                                    ):
                                        label = entity_type_object["label"]
                                        value = component_entity["data"][
                                            "skos:prefLabel"
                                        ]["@value"]
                    elif (
                        "Entiteit.type" in component
                        and isinstance(component["Entiteit.type"], list)
                        and len(component["Entiteit.type"]) == 2
                        and (
                            (
                                "@id" in component["Entiteit.type"][0]
                                and "https://stad.gent/id/concept"
                                in component["Entiteit.type"][0]["@id"]
                            )
                            or (
                                "@id" in component["Entiteit.type"][1]
                                and "https://stad.gent/id/concept"
                                in component["Entiteit.type"][1]["@id"]
                            )
                        )
                    ):
                        for entity_type_object in component["Entiteit.type"]:
                            if (
                                "@id" in entity_type_object
                                and "cest:" in entity_type_object["@id"]
                            ):
                                label = entity_type_object["label"]
                            elif (
                                "@id" in entity_type_object
                                and "https://stad.gent/id/concept"
                                in entity_type_object["@id"]
                            ):
                                thesaurus_id = base64.b64encode(
                                    entity_type_object["@id"].encode("ascii")
                                ).decode("ascii")
                                logger.info(thesaurus_id)
                                if thesaurus_id in thesaurus_cache:
                                    component_entity = thesaurus_cache[thesaurus_id]
                                else:
                                    component_entity = collection_api.get_object(
                                        "entities", thesaurus_id
                                    )
                                    thesaurus_cache[thesaurus_id] = component_entity
                                value = entity_type_object["skos:prefLabel"]["@value"]
                    elif relation_field != "Classificatie.toegekendType":
                        component_id = (
                            object_id
                            + ":"
                            + relation["subtype"]
                            + ":"
                            + str(sub_id_count)
                        )
                        logger.info(component_id)
                        component_entity = collection_api.get_object(
                            "entities", component_id
                        )
                        if not component_entity:
                            component_entity = collection_api.add_object(
                                "entities",
                                mappers.construct_component(
                                    component, relation["subtype"], component_id
                                ),
                            )
                        else:
                            updated_component_entity = mappers.construct_component(
                                component, relation["subtype"], component_id
                            )
                            updated_component_entity.pop("identifiers", None)
                            component_entity = collection_api.update_object(
                                "entities", component_id, updated_component_entity
                            )
                        map_relations(component_entity, component_id)
                        if component_entity and "@type" in component_entity["data"]:
                            label = get_label_by_type(
                                component_entity,
                                component_entity["data"]["@type"],
                                relation_field,
                            )
                            value = get_value_by_type(
                                component_entity, component_entity["data"]["@type"]
                            )
                        elif component_entity and "Entiteit.type" in component:
                            label = component["Entiteit.type"]["label"]
                            value = component_entity["data"][
                                "Classificatie.toegekendType"
                            ]["label"]["@value"]
                        sub_id_count = sub_id_count + 1
                    if component_entity and "_id" in component_entity:
                        collection_api.update_object_relations(
                            "entities",
                            object_id,
                            [
                                {
                                    "label": label,
                                    "value": value,
                                    "key": component_entity["_id"],
                                    "type": "components",
                                }
                            ],
                        )
        elif relation["type"] == "person":
            if relation_field in asset:
                person = asset[relation_field]
                person_id = base64.b64encode(
                    person["equivalent"]["@id"].encode("ascii")
                ).decode("ascii")
                if person_id in person_cache:
                    person_entity = person_cache[person_id]
                else:
                    person_entity = collection_api.get_object("entities", person_id)
                if "_id" not in person_entity:
                    post_data = {
                        "identifiers": [person_id],
                        "object_id": person_id,
                        "type": "person",
                        "metadata": [
                            {
                                "key": "fullname",
                                "label": "volledigeNaam",
                                "value": person["equivalent"]["label"]["@value"],
                                "lang": person["equivalent"]["label"]["@language"],
                            }
                        ],
                        "data": {
                            "volledigeNaam": person["equivalent"]["label"]["@value"]
                        },
                    }
                    person_entity = collection_api.add_object("entities", post_data)
                    person_cache[person_id] = person_entity
                if "volledigeNaam" in person_entity["data"]:
                    fullname = person_entity["data"]["volledigeNaam"]
                elif "volledigenaam" in person_entity["data"]:
                    fullname = person_entity["data"]["volledigenaam"]
                elif "fullName" in person_entity["data"]:
                    fullname = person_entity["data"]["fullName"]
                elif "fullname" in person_entity["data"]:
                    fullname = person_entity["data"]["fullname"]
                else:
                    fullname = ""
                collection_api.update_object_relations(
                    "entities",
                    object_id,
                    [
                        {
                            "label": person["Entiteit.type"]["label"],
                            "value": fullname,
                            "key": person_entity["_id"],
                            "type": "components",
                        }
                    ],
                )
        elif relation["type"] == "gender":
            if relation_field in asset:
                logger.info(asset[relation_field])
                gender_id = base64.b64encode(
                    asset[relation_field].encode("ascii")
                ).decode("ascii")
                if gender_id in gender_cache:
                    gender_entity = gender_cache[gender_id]
                else:
                    gender_entity = collection_api.get_object("entities", gender_id)
                if "_id" not in gender_entity:
                    value = rdf_helper.get_pref_label(asset[relation_field])
                    post_data = {
                        "identifiers": [gender_id, asset[relation_field]],
                        "object_id": gender_id,
                        "type": "gender",
                        "metadata": [
                            {
                                "key": "title",
                                "label": "titel",
                                "value": value,
                                "lang": "nl",
                            }
                        ],
                        "data": {"titel": value},
                    }
                    gender_entity = collection_api.add_object("entities", post_data)
                    gender_entity[gender_id] = gender_entity
                collection_api.update_object_relations(
                    "entities",
                    object_id,
                    [
                        {
                            "label": relation_field,
                            "value": gender_entity["data"]["titel"],
                            "key": gender_entity["_id"],
                            "type": "components",
                        }
                    ],
                )
        elif relation["type"] == "sub_assets":
            if relation_field in asset:
                for sub_asset_ld in asset[relation_field]:
                    if (
                        "/archiefgent" in sub_asset_ld["@id"]
                        or "/dmg" in sub_asset_ld["@id"]
                        or "/stam" in sub_asset_ld["@id"]
                        or "/industriemuseum" in sub_asset_ld["@id"]
                        or "/hva" in sub_asset_ld["@id"]
                    ):
                        sub_asset_id = base64.b64encode(
                            sub_asset_ld["@id"].encode("ascii")
                        ).decode("ascii")
                        if sub_asset_id in asset_cache:
                            sub_asset_entity = asset_cache[sub_asset_id]
                        else:
                            sub_asset_entity = collection_api.get_object(
                                "entities", sub_asset_id
                            )
                        if "_id" not in sub_asset_entity:
                            post_data = {
                                "identifiers": [sub_asset_id],
                                "type": "asset",
                                "metadata": [],
                                "data": {},
                            }
                            sub_asset_entity = collection_api.add_object(
                                "entities", post_data
                            )
                            asset_cache[sub_asset_id] = sub_asset_entity

                        collection_api.update_object_relations(
                            "entities",
                            object_id,
                            [
                                {
                                    "label": relation_field,
                                    "value": sub_asset_ld["MensgemaaktObject.titel"]
                                    if "MensgemaaktObject.titel" in sub_asset_ld
                                    else "",
                                    "key": sub_asset_entity["_id"],
                                    "type": "components",
                                }
                            ],
                        )


def get_value_by_type(object, type):
    if (
        type == "Activiteit"
        and "data" in object
        and "Entiteit.beschrijving" in object["data"]
    ):
        return object["data"]["Entiteit.beschrijving"]["@value"]
    if type == "MaterieelDing":
        return object["data"]["Entiteit.beschrijving"]
    if type == "GecureerdeCollectie":
        return object["data"]["Collectie.naam"]
    if type == "Classificatie.toegekendType":
        return object["data"]["skos:prefLabel"]["@value"]
    if type == "TypeTechniek" and "Entiteit.type" in object["data"]:
        for type_object in object["data"]["Entiteit.type"]:
            if "skos:prefLabel" in type_object:
                return type_object["skos:prefLabel"]["@value"]
        return object["data"]["Entiteit.type"]
    return type


def get_label_by_type(object, type, relation_field):
    if type == "TypeTechniek" and "Entiteit.type" in object["data"]:
        for type_object in object["data"]["Entiteit.type"]:
            if "@id" in type_object and "cest:" in type_object["@id"]:
                return type_object["label"]
    return relation_field


def parse_page(url, object_id_prefix, private):
    if private:
        data = requests.get(url, headers={"apiKey": os.getenv("API_KEY")}).json()
    else:
        data = requests.get(url).json()
    assets = data["@included"]
    object_id_prefix = f"{object_id_prefix}:"
    for asset in assets:
        logger.info(asset["@id"])
        if asset["@type"] == "Agent":
            save_person(asset, object_id_prefix, private)
        elif asset["@type"] == "skos:Concept":
            save_thesaurus(asset, object_id_prefix, private)
        else:
            save_asset(asset, object_id_prefix, private)
    return data


def get_next_url(page, stream_item):
    next_url = next(
        (
            x["tree:node"]
            for x in page["tree:relation"]
            if x["@type"] == "tree:GreaterThanOrEqualToRelation"
        ),
        None,
    )
    if next_url:
        stream_item["latest_timestamp"] = next_url.split("?generatedAtTime=")[-1]
        collection_api.update_object(
            "key_value_store", stream_item["object_id"], stream_item
        )
    return next_url, stream_item


def save_assets(url, stream_item, object_id_prefix, private):
    page = parse_page(url, object_id_prefix, private)
    url, stream_item = get_next_url(page, stream_item)
    while url:
        page = parse_page(url, object_id_prefix, private)
        url, stream_item = get_next_url(page, stream_item)


def start_import(argv, stream_endpoint, stream_id, object_id_prefix, private):
    new_stream_item = {
        "identifiers": [stream_id],
        "latest_timestamp": "2021-01",
        "object_id": stream_id,
    }
    if not (stream_item := collection_api.get_object("key_value_store", stream_id)):
        stream_item = collection_api.add_object("key_value_store", new_stream_item)
    elif "--reset" in argv:
        stream_item = collection_api.update_object(
            "key_value_store", stream_id, new_stream_item
        )
    stream_url = f'{stream_base_urls["public"]}{stream_endpoint}?generatedAtTime={stream_item["latest_timestamp"]}'
    if private:
        stream_url = f'{stream_base_urls["private"]}{stream_endpoint}?generatedAtTime={stream_item["latest_timestamp"]}'
    save_assets(stream_url, stream_item, object_id_prefix, private)


def main(argv):
    for stream_endpoint, stream_id in sync_streams.items():
        start_import(argv, stream_endpoint, stream_id, stream_id, False)
    for stream_endpoint, stream_id in async_streams.items():
        private = False
        object_id_prefix = stream_id
        if isinstance(stream_id, dict):
            private = stream_id.get("private", False)
            object_id_prefix = stream_id["object_id_prefix"]
            stream_id = stream_id["stream_id"]
        if private and skip_private_streams:
            continue
        Process(
            target=start_import,
            args=(argv, stream_endpoint, stream_id, object_id_prefix, private),
        ).start()


if __name__ == "__main__":
    main(sys.argv[1:])
