from wikidata.client import Client

default_lang = "nl"


class WikidataResolver:
    def __init__(self, collection_api):
        self.client = Client()
        self.collection_api = collection_api

    def get_entity_from_wikidata(self, qid, type, id=None):
        wikidata_entity = self.client.get(qid, load=True)
        try:
            description = wikidata_entity.description[default_lang]
            label = wikidata_entity.label[default_lang]
        except AssertionError:
            return None
        entity = {
            "type": type,
            "object_id": qid,
            "metadata": [
                {"key": "title", "value": label, "lang": default_lang},
                {"key": "description", "value": description, "lang": default_lang},
            ],
        }
        if id:
            entity["identifiers"] = [id]
        return entity

    def get_and_update_wiki_data_entity_from_db(self, museum_id):
        if not self.collection_api.get_object("entities", museum_id):
            museum_entity = self.get_entity_from_wikidata(
                museum_id, "museum", museum_id
            )
            if museum_entity:
                museum_entity = self.collection_api.add_object(
                    "entities", museum_entity
                )
        else:
            museum_entity = self.get_entity_from_wikidata(museum_id, "museum")
            if museum_entity:
                museum_entity = self.collection_api.update_object(
                    "entities", museum_id, museum_entity
                )

        return museum_entity
