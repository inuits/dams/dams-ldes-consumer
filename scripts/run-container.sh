#!/bin/bash
DOCKER=docker
if [ -x "$(command -v podman)" ]; then
  DOCKER=podman
fi

${DOCKER} run -it --rm --env APP_ENV=production inuits-dams-ldes-import-service:latest $@
