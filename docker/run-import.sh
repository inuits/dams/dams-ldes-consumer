#!/bin/sh

export OPTIONS=$1

cat << EOF
=========================
== Running LDES import ==
=========================
EOF

python api/dams-ldes-import-service.py $OPTIONS

cat << EOF
==========================
== LDES import finished ==
==========================
EOF
